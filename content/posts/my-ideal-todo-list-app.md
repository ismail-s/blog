+++
date = "2016-02-22T12:54:38"
draft = false
title = "My ideal todo list app"
tags = [ "Ideas" ]
+++

The todo list app has been done again and again and again, yet I always seem to have some gripe with each of the ones I have used. ATM, I use Google Keep as it gets most things right. What follows are some of the features that I deem important for such an app to have:

1. Queues.
    1. Sequence of subtasks that are dependent on the previous one being completed.
    2. Better would be graph support. And then the usual decision tree analysis to find the critical path, max/min time to do all tasks.
    3. Of course, this can get quite complicated, and it would have to be kept as simple as possible whilst still being very flexible.
2. Tags
    1. Basically being able to group tasks, but more flexible than mutually exclusive categories.
3. Reminders
    1. This is a killer feature, and the reason why I don't use other tools eg Trello, evernote.
    2. Reminders have to be customisable. I should be able to set a specific time/day, and be able to recur the reminder, and have sensible defaults for saying that "I'll do it in an hour, not right now" when a reminder comes up.
    3. IMO, Google keep does this quite well.

Before you ask, I have no plans to actually create yet-another-todo-list-app. It feels like it is way too crowded at the moment, and I feel more that I simply haven't tried enough to find the best one for me.

And if you didn't notice, I find that nested lists can be a nice way to organise thoughts about a particular topic. Give it a try if you haven't already.
