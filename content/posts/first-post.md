+++
date = "2016-09-03T15:06:41Z"
draft = false
title = "Switching to Hugo"
tags = [ "Blog" ]
+++

Over the course of my life, I have used various blogging framework thingies. IIRC I have used
wordpress and blogger at some points in the past, followed by
[fireblog](https://www.github.com/ismail-s/fireblog), which was made by me. I have had several
issues with fireblog:

1. It is dynamic. I could change it into an SSG or a static/dynamic hybrid thing, but I can't be
   bothered at this point in time.
1. It is slow. The reason for this is that the server has been on a slow connection, on a slow
   server with a dynamic ip address.

So, I am now switching to [hugo](https://gohugo.io/), hosted on gitlab pages. So I don't have to
worry about hosting costs, and I get a versioned history of everything. And hugo has lots of themes.

The downside is I don't get comments, unless I use disqus (which I am on the fence about). But meh.

If you're curious, the source code for this new blog is [here](https://www.gitlab.com/ismail-s/blog).
