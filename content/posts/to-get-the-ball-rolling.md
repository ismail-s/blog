+++
date = "2014-11-18T19:27:30"
draft = false
title = "To get the ball rolling..."

+++

In the name of Allah, the most gracious, most merciful.

So you can now infer my religion. That doesn't mean you know my religion, but that you can make a good guess that is most probably right. This is odd. I mean, why would I even think to start doing this. I mean, 750 words! 750. No kidding. No fudging. Where am I going to find the time for it?!

Man, only 74 words! My gosh, this will be a mission.

But you see, I do think I have enough time every day to write 750 words. And I don't see it as a waste of time. Not at the moment, anyway. See, I always have these thoughts, that I never write down. And they go off into the nether (but they don't go into the void). I expect that you also have the same problem, that of thinking of some cool idea, but not writing it down and forgetting about it. My current idea is that this will help me keep track of them. We'll see how that goes.

Apparently, this website gives you points for writing, and rewards you for daily writing. So it can help to develop discipline. That sounds good to me.

See, I really want to discipline myself more. By this, I mean physical, mental, moral and physical discipline. I want to be the person about whom nobody can say ill of. I want to be known, not as the lazy, unkind, unhappy person, but as the one upon whom you can depend on. I want to be the one who people know will get the job done, and on time. I want to bring happiness into as many people's life as possible.
And that sounds really good. But that's where it stops.

See, that's all thoughts, words, half-hearted intentions. No, I don't even think I can permit myself to call them full-on intentions, not until I seriously start working towards improving my discipline. And this is what is frustrating me.

Procrastination.

Just doing pointless things, distracting myself, trying not to get the task done.
And now I feel I must say something very odd.
Something rather odd indeed.
I owe you an apology.
Well, not you, but me.

See, I could be __so much more__.

__So much more.__

And I'm not. Not yet, at least. But I keep telling myself that I will be. I have these pictures in my head of some future me, some future me that seems amazing, that can save the day, that people look up to, that does the coolest of things. And deep down I know, I __know__, that unless I make some effort, I haven't any hope of getting there.

There is a dream. Not a well defined one, but a dream. And it keeps evolving over the years. But I am not sure I can get to it. Not like this, at least.

See, one was one, but one is not what one was.

You are you, and you were you, but you are not what you were back then. You changed since than. You are not the same person you were 5, 10, 15 years ago.
But when I think back to me as a child, I feel that I __owe__ him a better me. For his sake, for the child that turned into me, I must make that effort to better myself, to discipline myself, to make it so that I can run faster for longer (walking up stairs at uni has helped with that), so that I can lift more, so that I can calm myself down, so that I can cope with being around people and with being alone, so that I can focus on one task (like I am doing so typing this soliloquy) and get it out of the way.

I am sure I have the time. I think I have the ability, or at least, cannot see why I wouldn't. I just need to __snap__ into it, to get the ball rolling.

Hmmm, maybe that could be the title of this piece, 'to get the ball rolling'. Yeah, that sounds quite cool to me.

Erm, so, yeah, that's just a bit about me. Doesn't it seem funny, how many people there are around the world, with all their thoughts brimming in their heads. Who hears them? Who listens to them all? Well, if you ask me, there is one quite simple answer.
Allah.



_This was a piece that I wrote on [750words.com](http://750words.com). On that website, you are meant to write 750 words a day._