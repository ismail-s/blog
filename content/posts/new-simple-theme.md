+++
date = "2016-01-03T15:00:30"
draft = false
title = "New Simple Theme"
tags = [ "Blog", "Theme" ]
+++

I've just merged in a new theme, which I've called "simple". Because it is meant to be dirt simple. And it is. For at least the next few hours, this blog will provide a demo of it. There are still various tweaks I should make to it, maybe add a splash of colour, resize things, but it at least passes the tests.
