+++
date = "2014-12-29T12:38:28"
draft = false
title = "Tales from Zims-part 2"
tags = [ "Tales from Zims" ]
+++

Here is the second part of my tales from Zims. Note that this is a reposting of a post I wrote in December 2012.

Now, as of writing this post, I am not a well-travelled person, and have seen very few cities. To me, Harare was extremely busy, overflowing to the seams with cars, commuter buses and pick-ups. This is what you get when there is no public transport, and the state of the roads made this no better. Combined with bad driving (the commuter buses again) and an erratic power supply, and you end up with the worst transport system I have laid my eyes on. Seriously. Locals there told me that it wasn't always like this, that they used to work in the city and come home for lunch. Now, this would take far too long.

By this point, I expect you'll have vowed never to set foot in Harare or anywhere in Zimbabwe, and I totally understand where you're coming from. The thing is, I could end the post here, but to do so would be to miss out an entirely different side to Zimbabwe.
Did you notice during the London Olympics, that the USA, one of the most prosperous nations, did not even have any decent coverage of the greatest sports event in the world? Zimbabwe did. It may have been through paid satellite TV that most in Zimbabwe wouldn't have access to, but it was there, providing a window back to my home country as they hosted a very impressive sporting celebration. *(Update 2014: when I visited the country this year, I noticed that they had the world cup, or at least some of the matches on the state-owned broadcaster, which is a free channel. This could well have been the case in 2012.)*

Also, did you know that the widest waterfall in the world is in Zimbabwe? Sure, you can take a look at it from Zambia, but the best views are just over the border. Due to the popularity of the falls, this is probably one of the best kept places in the country, even boasting cats eyes on some of the roads (seriously, it is a brilliant place). Apparently, there are South African package holidays that include the Victoria Falls, which can mean people potentially visiting Zimbabwe without knowing it.

In many places around the world, there is a clash between modernisation and preserving the natural treasures of the country. In Zimbabwe, the state of the country keeps the natural treasures in their pristine state, which can be sort of seen as one of the few 'benefits' of the state of the economy (this is quite dubious sounding-Zimbabwe can become richer without any compromise needed, and many modern countries do balance modernisation and conservation). *(Another update from 2014: there are various issues with the government around mining and their relationship with the Chinese government. I imagine you can paint the picture for yourself as to what sort of things will be going on here.)*

There is one final story I have to share with you, but that is for a later post. Stay tuned!
