+++
date = "2015-09-20T20:41:03"
draft = false
title = "Using quip to write a report"
tags = [ "Latex", "Quip", "Uni" ]
+++

Over the past few months, I have been writing a report on what I was doing during my year on placement. During this year I have mainly been testing a booking system for the NHS, but I am not talking about what I did during the year in this post._(I also am not sharing the report here, but if you [email](http://www.personal.leeds.ac.uk/~cm12is) me, I may be able to give it to you.)_ Instead, I want to talk about how I made the report.

Initially, I started off using quip, as it's cross-platform, has document history, looks nice and allows you to export your documents in a variety of formats. Oh, and did I say it's free unless you want a business version?

However, quip has its limitations. For example, it only has three levels of headings, which definitely limits you. You are also limited in terms of formatting, although this was less of an issue to me at the time.

I also decided, once I'd written most of the report, that I might try and turn the report into latex. Mainly because it looks much more professional, but also because it would allow for more customisation, making it easy to do cross-referencing and citations.

Quip has a latex export feature, which I used, but I then had to go through and get rid of a bunch of stuff that they had in their exported latex version to get it to be how I wanted it to be.

Now, all I had to do was some tidying up. But if anyone has heard me talk about latex, they'll know that I think of it as an old system with lots of annoying baggage and verbose syntax. But it does the job and is the defacto tool, so meh, I got my hands dirty.

What this whole process essentially boiled down to was that I used quip as a sort-of gui front end to latex. This worked, but there was plenty of work required to make the latex version be how I wanted it to be. I think in the future though, I'd create a git repo and find a suitable latex gui program instead.

I might try and reword this post at some point in the future.
