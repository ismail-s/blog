+++
date = "2014-11-18T19:35:32"
draft = false
title = "Some completely random thoughts on nature"

+++

On rare occasions, I have looked around me, at the buildings rising high up in this city, and I have wondered. I have wondered what we have done, what we humans have achieved by doing this. I'm no hippie, but I don't think the answer lies in trying to cover the world with tarmac and concrete. I don't think having a couple of trees in the pavement is enough.

You see, it requires a different perspective, in my opinion, one which I feel we have lost in the developed world, and one which the developing world is in the process of losing. Nature is not there to be tamed, but to be _embraced_. Treehugger, indeed.

Why don't we have grass on our pavements to walk on, as opposed to tarmac. Why restrict nature to delineated parks, when it can be an integral part of our cities, weaving through all our buildings.

Why are there too few roof gardens? Could we not incorporate trees and plants into our buildings, so that when I am sitting in the office, I see perhaps a creeper growing on the walls, or a tree within the building.

Now, this may seem like an odd view for me to hold, given my immersion in science and maths, but I do not see science and nature as contradictory. Science is not just for taming nature.  Rather, the two should be mixed together, with scientists, and all people for that matter, having a deep love and connection with the outdoors, not feeling that we need to shut it out of our houses, but that we should open the door to nature.

And no, I don't think you should only choose to have plants that look beautiful in your house, but my views on that are complicated and deserve another post.

Stay tuned!

_PS- Above, I have mentioned having grass on our pavements. looking back, that's not a good idea as you'll just be carrying mud into buildings. But I still am against the concrete jungle (in a manner, as I shall elaborate on in the next post)._