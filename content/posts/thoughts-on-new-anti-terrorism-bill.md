+++
date = "2015-02-17T22:38:31"
draft = false
title = "Thoughts on a new anti-terrorism bill"
tags = [ "Politics" ]
+++

The scary part is the predictability.

I have been hearing on the news about the rise of smaller political parties, how the extreme parties are gaining more support.

This is not new. Remember before the second world war, how extreme right parties grew in power in places like Germany, and how the extreme left grew in Russia.

But let me repeat-what is scary is the predictability.

So the question that comes to mind is is this a short term trend, or will there be long term ramifications. I expect the latter.

You see, inexperienced parties gaining power will inevitably result in some bad decisions being made. Whilst I'm not trying to stick up for mainstream parties, they have more experience of power, so seem to want to rock the boat less, so to speak. It is precisely these kinds of promises by the smaller parties, that they promise to rock the boat, that gets them power.

Of course, on the other hand, if all we do is try and maintain the status quo, then we're effectively sticking our fingers in our ears and hoping all our problems will go away.

They won't.

Then the sensible answer that emerges, as always, is the middle ground. To keep things the same but with an element of change.

Note how my conclusion sounds so very similar to professor Umbridge's speech in the fifth Harry Potter book. I have no idea what to conclude from that...
