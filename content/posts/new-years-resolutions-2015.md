+++
date = "2015-01-07T17:52:54"
draft = false
title = "New year resolutions-2015"

+++

I have ended up with some new years resolutions this year. In an attempt to get myself to actually stick to them, I'm writing them here:

1. Talk clearer
1. Get things done aka stop putting things off all the time
1. Form good habits eg regular exercise