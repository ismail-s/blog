+++
date = "2015-03-21T12:53:26"
draft = false
title = "On the benefits of writing"
tags = [ "Writing" ]
+++

I didn't like English at school. I suspect I'm not alone. In one of the books I recall reading at school (I've forgotten which), in the preface there was mention of how forcing children to study a book, to write essays about it, causes them to dislike the book. The feelings of annoyance, of frustration at homework, become partly directed at the book.

You see, it wasn't that I didn't enjoy reading. One of the many gifts granted to me by my mother was a love of reading. But analysing a book, writing an essay, these detracted from the enjoyment of reading.

And then I got older. And I now didn't have to do English. Not officially, anyway. But that wasn't the end of writing for me. Far from it. Instead, I started writing blogs. I wasn't punctual, but then I wasn't under deadlines. I tended to type, due partly to this being quicker than writing. Of course, I had the usual issue of getting in the right frame of mind. But every so often it would be that perfect moment, when the ideas would flow, and I would construct a stream of thought that, I felt, truly reflected my beliefs.

You see, whilst I may have disliked the subject, I recognised it's power and value.

You see, writing can force you to critically evaluate your ideas. To think about things to a greater level than what you would do in your head.

So, like in other areas, I've kind of come full circle. The issue I have with blogs is coming up with ideas and finding time. And writing good blog posts. We'll see whether I deal with all these problems.
