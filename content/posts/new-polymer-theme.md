+++
date = "2015-07-09T01:29:43"
draft = false
title = "New polymer theme"
tags = [ "Blog", "Theme" ]
+++

OK, so I have been working a new polymer theme for this blog. I have just deployed to production and already have found various issues with it. The worst issues affect me.

Of  course, i did test the website beforehand. The issues i am finding now are around performance and usability on a mobile. The performance, i have a quick fix that should go partway towards fixing the problem. But the real problem is probably  that using polymer results in a lot of dependencies.

Anyways, we'll see how things go when i get time to work on the issues.

**UPDATE 12/7/15:** Shortly after I made this post, I fixed the issue around scrolling not working. And tried to speed the website up slightly by playing with some config settings. And I resized the title to make the header bar look a bit better IMO. However, I expect that HTTP2 will give a speedup, whenever nginx get it developed.

**UPDATE 17/7/15** I have added some more caching. However there are bugs in it I think. But they shouldn't affect anyone except me. I have started to understand how much of a pain caching can be and how much of a speedup it can give.
