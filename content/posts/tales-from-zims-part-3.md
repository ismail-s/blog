+++
date = "2015-01-02T18:35:47"
draft = false
title = "Tales from Zims-part 3"
tags = [ "Tales from Zims" ]
+++

So here is my final post in this series about Zimbabwe. I'll probably have to edit most of this post at some point.

One of the things I sometimes wonder is if Zimbabwe will ever get out of its current predicament. There is an opposition party, and in general I would expect not many people to support the current government. But it feels like change will only come when the current leader passes away.

The real question, at that point, is what fills the void. Already, there are movements within Zanu PF with people trying to position themselves such that they benefit from mugabe's death. The big worry is that the transition will be messy. That ordinary people will suffer. But that is the worry with so many political transitions.

The odd thing about the current situation is that things are relatively peaceful. Compared to places like south Africa, at least. People still live behind gates and walls topped with barbed wire, but shootings are not too common, from my experience (on the other hand, people getting run over by some government related car does happen at times).

So, to summarise, I would like to say that I'm optimistic about Zimbabwe's future.
But I'm not. It doesn't look good at the moment. But on the other hand, depending on what happens when the current leader is gone, things _may_ improve.

We'll see.
