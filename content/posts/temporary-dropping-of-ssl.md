+++
date = "2015-08-17T11:53:07"
draft = false
title = "Temporary dropping of SSL"
tags = [ "Blog" ]
+++

You may have noticed that the website no longer is using ssl/tls. Before, I was using cloudflare. On their end, there were some issues which have meant that I've decided to remove them from the loop for the moment. Of course, this does remove a layer of protection, but I am relying on extremely low traffic. And this is definitely not a mission critical website by any means.
Anyways, I have been working no decoupling the code for this website to make it easier to maintain. And hopefully, it will allow me to have more fine-grained caching too. I have also increased the thoroughness of the automated testing and just basically tried to do non-visible changes. So no functional changes whatsoever. There are still one or 2 more things to do before deploying this new code, and I need to fit this in around my other commitments. Meh.

Belated update 16/9/15: Yeah, I'm back on cloudflare. Apparently the ssl issue is to do with certain browsers not liking [SNI](https://en.wikipedia.org/wiki/Server_Name_Indication). Meh.
