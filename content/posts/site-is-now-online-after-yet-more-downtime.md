+++
date = "2015-09-27T21:56:32"
draft = false
title = "Site is now online (after yet more downtime)"
tags = [ "Blog" ]
+++

Yes, I know, for the past few days the site has been offline. I played around with partitions, and as always assumed that I would be able to get the site up and running in only a few hours.

How wrong I was.

I had to compile python twice as I hadn't installed various packages before the first compilation. And the compilation takes ages. And then even the package server thing I was using went down for a while due to dns changes on their end.

On the other hand, I understand well how to get the site up and running. It is relatively straightforward, although I have made one change and am using cherrypy rather than uwsgi. More because I had trouble installing uwsgi. But meh.
