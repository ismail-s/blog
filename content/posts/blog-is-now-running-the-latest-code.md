+++
date = "2015-10-21T23:12:54"
draft = false
title = "Blog is now running the latest code"
tags = [ "Blog" ]
+++

Since I put the blog source code onto Github, I have been working on the code there, fixing bugs and stuff. With the code being on Github, I can work more easily and frequently on the code, but for the past few weeks I haven't been updating the code this blog itself is running, as it would require a bit of effort and I've been busy.

Well, I have just redeployed this blog with the latest code. I have fixed some bugs around caching, made changes to urls and how posts are looked up (a big change to make), started improving pages that show multiple posts, and other stuff. In short, I've been busy improving stuff. The funny thing is that you shouldn't really notice all of those things...
