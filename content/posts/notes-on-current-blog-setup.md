+++
date = "2015-06-24T14:54:55"
draft = false
title = "Notes on the current blog setup"
tags = [ "Blog" ]
+++

I thought it might be nice to give some information on how this blog is setup.

## Web framework
First off, the blog has been made using python, with the [pyramid application framework][pyramid]. I compared various python Web frameworks and pyramid seemed to have good documentation, plenty of support and a nice api. And they also have some nice tutorials on getting started, and some starter templates.

I considered [django] but pyramid seemed a lot more flexible. Things like being able to choose any database and template engine you want, without things being hard to get working together, were a draw for me.

## Servers
The nice thing about python is that most Web frameworks use [WSGI], which means you can switch between different Web application servers without too much fuss.

For development, I tend to use waitress as it is the default Web application server that comes with pyramid.

For production, I have used waitress, [cherrypy] and [uwsgi] at different times. At the time of writing I am using [uwsgi]. But they have all generally been ok to use.

In front of the Web application server I have nginx. Nginx is awesome, in terms of its size, speed and resource usage. It also has native support for the uwsgi protocol which is one of the reasons why I am using uwsgi as the Web application server.

More out of caution than anything else, I have the whole website sat behind [cloudflare].

## Dynamic DNS
My ip address keeps changing, so I have to use something like [ddclient] to keep cloudflare updated with my ip address. I don't like ddclient though (it's written in perl and is a pain to install manually) so I've written a basic python script to do the job for me instead. Nothing fancy, but it does the job.

[pyramid]: http://www.pylonsproject.org/projects/pyramid/about
[django]: https://www.djangoproject.com/
[WSGI]: https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface
[uwsgi]: https://uwsgi-docs.readthedocs.org/en/latest/
[cherrypy]: http://www.cherrypy.org/
[ddclient]: http://sourceforge.net/p/ddclient/wiki/Home/
[cloudflare]: cloudflare.com
