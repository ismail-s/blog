+++
date = "2015-10-06T22:55:30"
draft = false
title = "Source code for the blog is now on Github"
tags = [ "Blog" ]
+++

Yes, you read that correctly. The source code that runs this blog is now on [Github](https://www.github.com/ismail-s/fireblog). Unfortunately I am not good at coming up with names, and fireblog was the best I could think of.

Putting the source code on Github helps me in several ways:

1. Showing people the code is way easier.
1. Working on the code is possible from pretty much any decent computer with Internet.
1. Open source is cool and all that stuff...
