+++
date = "2014-12-04T00:24:11"
draft = false
title = "Multiple Personalities"

+++

>"If there is one thing I have learnt, ... it is to distinguish clearly between Rafa and Rafael. Rafa is the famous tennis player; Rafael, the name they call me at home, is the real one, the one who could have ended up doing something else altogether with his life and it would have made no difference.”

*FT Weekend magazine interview, 10 Jan 2014*

I like that quote. It highlights how people class their behaviours in different situations in different ways.

When someone is drunk, we say that they are not themselves. What do we mean by this? The ethanol does affect them to some extent, but this is mixed in with their personality, so they are still, to some extent, themselves.

Or take a person in anger. 'Seeing red' is quite an apt phrase-the anger pulses through you, your mind is focused purely on one thing (well, sort of-ish). Now, after that person has laid down for a while, he would not acknowledge his/her previous behaviour as him/herself. Yet, who else is it? If not purely them, then enough of it is them.

My point then, after rambling, is that you are all of you. You are who you are at home, work, on holiday, while stressed, while relaxed... In different scenarios different aspects of our character manifest themselves more than other aspects.

Yet why would we class different aspects of ourselves as not the real us? Shame? Embarrassment? Take a look at the above quote-I highly doubt there is either of those there.

So why do we do this then? Do we only see our 'real' self as being the aspects of our personalities that are manifest when we are with those whom we feel the most comfortable with i.e our family and/or friends. I don't know, to be honest, but I like thinking about these things, especially with people who also like to think. Seeing as you've got this far, I'd like to hear your thoughts on this-drop me an email at me @ this [TLD](http://en.wikipedia.org/wiki/TLD)