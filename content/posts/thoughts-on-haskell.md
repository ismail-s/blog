+++
date = "2016-07-04T07:54:59"
draft = false
title = "Thoughts on Haskell"
tags = [ "Haskell", "Programming" ]
+++

Over the past few months, I have been learning haskell. It is truly an amazing language, unlike the heavyweight languages (Java, Python, C#), with a lot of innovative features in it. This is probably due to it being originally (although it still is) a research language, though it does seem very capable for use in production. Here are a few of my thoughts about it so far.

* GHC, the standard compiler/interpreter, is awesome. The compiler times could be faster, but the executables it produces are very fast.
* The language is very beautiful. To give an example: `fibs = 0:1:(map (\n -> fibs!!n + fibs!!(n+1)) [0..])` is an infinite list of the Fibonacci numbers. It is not the fastest way of doing this (matrix exponentiation is really fast) but it is very good in terms of performance.
* The level of support for higher order functions and currying is really good. In particular, I like the way function types are expressed, eg `func :: Integer ->  Integer -> String` is a function that takes 2 numbers and returns a string. It can also be thought of (and used) as a function that takes one number and returns a new function, that takes one number and returns a string. This builtin currying allows for simplifying various problems in a beautiful way.
* The type system is really powerful. Writing functions that work on classes of types is very straightforward, and type inference means that a lot of the time, you don't need to write types. Although standard practice is to write out the types of functions, purely to help people reading the code. Often, you can just let ghc figure out what the type of a function is, and just copy paste it back into your code for documentation.
* There are a lot of extensions built into GHC, and these extensions can be enabled on a file by file basis by adding flags at the top of the file. 
 
I also have issues with haskell: 

* GHC is massive to download (btw use [stack](http://docs.haskellstack.org/en/stable/README/) to install haskell). Like, massive. I know that hard drives are bigger these days, that network speeds are faster, but I don't think that gives developers a right to make things massive when they don't need to be (at least, not for a few years). I don't use the most up to date devices, as I like to keep using older hardware rather than waste resources and money continually upgrading.
* The language can be hard to understand, and explanations found online can sometimes require understanding various bits of theory from maths. Now, I am a mathematician (not in terms of working as one but in doing maths at uni), but I prefer simple explanations at all times. And it annoys me when people complicate things for no reason, especially when creating learning materials, or APIs (if you want to see stupidly designed APIs, look at various Java libraries).
