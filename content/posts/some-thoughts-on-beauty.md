+++
date = "2014-11-23T12:32:14"
draft = false
title = "Some thoughts on beauty "

+++

Ok, as promised in the last post, here are my views on beauty.

I don't particularly focus obey on how I look (some would say that's an understatement). That doesn't mean I don't maintain myself, but that I don't buy fancy clothes or really fuss about what I wear to weddings. I don't feel too fussed.

I do notice other people are fussed, and I do notice other people commenting on other people's clothes. 

_"My gosh, what clothes is she wearing!"_

_"She/he looks so nice with that look,  with his/her hair done up like that."_

Don't expect to hear me saying that. It's not that I don't evaluate people's dress, but that I tend not to, unless they are dressing stupidly (which appears to be a lot of people), in which case I look away. Or I may feel a tad uncomfortable looking at people, so I won't.  So in general, I tend to look a lot at buildings, or at least non-people things.

Ok, let's just put it bluntly: _what do you care what other people dress like?_ Try and look inside them instead, rather than focusing on what clothes they're wearing. By not looking directly at people, I hope not to judge them as much by how they look, but I am sure I still judge them. Which annoys me. At times, I might get annoyed at the way a person dresses for its effect on me, but I must try to remember that I don't understand their situation, their circumstances. By now, you may have guessed my gender. S'not hard.


We look at things and get influenced by how they look, and have difficulty separating appearance from every other characteristic. This must be acknowledged, which is what I am trying to do here.

How many people would be willing to donate to WWF if their logo featured a worm, or a mosquito, or some other non-beautiful danlu (lojban for 'living thing'). Pictures are powerful, and we must acknowledge that power, and make sure it does not dominate us where it causes problems.

In other words, we care loads about what we see, how things look, without looking inside.

Hmmm, that was a lot of words to say the sayingː

>Don't judge a book by its cover.

I'll probably need to reword this post.



_Ps When you talk to someone online in a chatroom, you can go on for a while without knowing even what gender they are. Think about how you talk then and when you meet someone in person. Online chats can maybe remove your prejudices and show them clearly to you._